function physical_host? (){
  [[ "$(ps -p 1 -o comm=)" != 'systemd' ]] && echo 255 && return 255;
  echo 0
}