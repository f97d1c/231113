function cluster_execute_cmd (){
  # -n 参数: 需要防止读取标准输入 否则循环内将断开后续脚本执行
  ssh -n -o PasswordAuthentication=no -o StrictHostKeyChecking=no $1@$2 "$3"
  return $?
}