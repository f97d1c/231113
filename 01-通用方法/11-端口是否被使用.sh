function port_used? (){
  netstat -an | grep ":::$1" 1>/dev/null 2>/dev/null
  echo $?
}