#!/bin/bash --login

# source "$kun_main_path/lib/other/backup_file" $file_path

function backup_file(){
  backup_file="$1"
  if [ ! -f "$backup_file" ]; then
    output "被备份文件不存在: $backup_file"
    exit 255
  fi

  backup_file_md5_value=$(cat $backup_file | md5sum)
  backup_file_md5_value=${backup_file_md5_value:0:32}
  backup_path="${backup_file%.*}.$(date '+%Y-%m-%d-%H_%M_%S')-$backup_file_md5_value"

  # 如果文件后缀不为空则添加后缀
  if [[ ! "${backup_file##*.}" == "${backup_file}" ]];then
    backup_path+=".${backup_file##*.}"
  fi

  same_backup=($(find ${backup_file%.*}.*-${backup_file_md5_value}* 2>/dev/null))

  if [ ${#same_backup[@]} -ne 0 ]; then
    same_backup_md5_value=$(cat ${same_backup[0]} | md5sum)
    if [ "$backup_file_md5_value" == "${same_backup_md5_value:0:32}" ];then 
      output "跳过, 已存在相同内容备份: ${same_backup[@]}"
      return 0
    fi
  fi

  output "备份文件: $backup_file"
  sudo cp $backup_file $backup_path
  code=$?
  if [[ ! $code -eq 0 ]]; then
    output "$backup_file 文件备份异常, code: $code"
    exit 255
  fi
  output "完成备份: $backup_path"
}