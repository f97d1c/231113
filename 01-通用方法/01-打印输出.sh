#!/bin/bash --login

function output (){
  printf "[$(date '+%Y-%m-%dT%H:%M:%S.%3N')] "
  printf "$*"
  printf "\n"
}