function check_md5(){
  [ ! -f "$1" ] && exit 233;
  [ ! -f "$2" ] && exit 233;

  f1_md5_value=$(cat "$1" | md5sum)
  f1_md5_value=${f1_md5_value:0:32}

  f2_md5_value=$(cat "$2" | md5sum)
  f2_md5_value=${f2_md5_value:0:32}

  [ "$f1_md5_value" == "$f2_md5_value" ] && return 0

  return 1
}