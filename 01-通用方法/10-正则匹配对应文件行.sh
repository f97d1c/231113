function get_line_by_match(){
  result=$(sed -n "/$2/p" $(get_path_by_name "$1"))
  
  [[ $? -ne 0 ]] && exit 233
  [[ $(echo -e "$result" | wc -l) -ne 1 ]] && exit 233

  echo $result
}