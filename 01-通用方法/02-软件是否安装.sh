function installed? (){
  which "$1" 1>/dev/null 2>/dev/null
  [[ $? -eq 0 ]] && echo 0 && return;

  res=$(dpkg -l "$1")
  [[ $? -ne 0 ]] && echo 255 && return;
  # 期望状态:(i) 状态:已安装(i)
  [[ $(echo "$res" | grep ^ii 1>/dev/null 2>/dev/null; echo $?) -eq 0 ]] && echo 0 && return;

  echo 255
}