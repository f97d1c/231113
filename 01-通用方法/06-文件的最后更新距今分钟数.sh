function last_change_ago_m (){
  # 获取文件的最后更新时间
  last_update_time=$(stat -c %Y "$1")
  # 获取当前时间
  current_time=$(date +%s)

  # 计算文件最后更新时间截止目前的秒数
  seconds_since_update=$((current_time - last_update_time))

  echo $((seconds_since_update / 60))
}