# fsv: file storage variable

# 新增数组元素
function fsv_array_item_add () {
  file_path=$(get_path_by_name "$1")
  # echo "$2"
  if [ ! -s "$file_path" ]; then
    echo "('$2')" | tee $file_path
    return 0
  fi
  
  pattern='^\(.*\)$' 
  if [[ ! $(cat $file_path) =~ $pattern ]]; then
    echo '[false, "文件内容非数组格式"]'
    exit 233
  fi

  eval "fsv_array=$(cat $file_path)"

  echo "${fsv_array[@]}" | grep -qw "$2" >/dev/null; 
  [[ $? -eq 0 ]] && return 0;

  old_text=')'
  new_text=" '$2')"
  sed -i "1s/$old_text/$new_text/" "$file_path"
}