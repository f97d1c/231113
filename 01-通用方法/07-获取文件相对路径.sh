function get_path_by_name () {
  if [[ "$2" == '文件夹' ]];then
    result=$(find -name "$1" -type d)
  else
    result=$(find -name "$1" -type f)
  fi

  [[ "$result" == '' ]] && echo '[false, "获取文件相对路径返回结果为空"]' && exit 233
  [[ ! $(wc -l <<< $result) -eq 1 ]] && echo '[false, "获取文件相对路径返回结果不唯一", "'$result'"]' && exit 233
  echo $result
}