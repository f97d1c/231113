[[ "$1" =~ 'debug' ]] && return 8;

if [[ "$(ps -p 1 -o comm=)" == 'systemd' ]];then
  last_update_time=$(stat -c %Y "/var/cache/apt/pkgcache.bin")
  [ ! $(($(($(date +%s) - last_update_time)) / 60)) -lt 10 ] && return 8;
fi

output '清理所有软件缓存'
sudo apt-get autoclean
sudo apt-get clean

output '删除系统不再使用的孤立软件'
sudo apt autoremove -y

# 就此镜像而言,这条命令在添加后可以使镜像减小423MB
if [[ ! $(physical_host?) -eq 0 ]];then
  output '删除所有apt/lists下的文件'
  sudo rm -rf /var/lib/apt/lists/*
fi

output '清理孤立的库文件'
sudo apt-get -y remove --purge