[[ "$1" =~ 'debug' ]] && return 8;

if [[ "$(ps -p 1 -o comm=)" == 'systemd' ]];then
  # 获取文件的最后更新时间
  last_update_time=$(stat -c %Y "/var/cache/apt/pkgcache.bin")
  # 距离上次更新小于10分钟则更新
  [ ! $(($(($(date +%s) - last_update_time)) / 60)) -lt 10 ] && return 8;
fi

output '软件升级'
sudo apt upgrade -y