
# 关于

## 官方安装源

Deepin v20+是基于unstable分支点Debian 10(buster)的衍生版本,<br>
所以官方的Docker源安装方式是不支持Deepin v20+的,<br>
所以需要对官方提供的安装方式进行改进才能安装免费社区版的Docker安装到Deepin v20+上面.

# 步骤

## 卸载旧版本

```sh
sudo apt remove -y docker.io docker-engine
```

## 获取Docker源的Https证书

```sh
curl -fsSL https://mirrors.ustc.edu.cn/docker-ce/linux/debian/gpg | sudo apt-key add -
```

## 添加source.list文件

```sh
echo 'deb [arch=amd64] https://mirrors.ustc.edu.cn/docker-ce/linux/debian buster stable' | sudo tee /etc/apt/sources.list.d/docker.list
```

## 安装docker-ce

```sh
sudo apt update && sudo apt install docker-ce -y
```

## 检查安装的Docker版本

```sh
docker --version
```

# 参考资料

> [CSDN | Deepin Linux v20+ 安装Docker最新版的方法](https://blog.csdn.net/richie696/article/details/113078983)