# 是否为物理宿主机环境
[[ ! $(physical_host?) -eq 0 ]] && return 8;
# 是否已安装
[[ $(installed? 'docker') -eq 0 ]] && return 8;

list_file='/etc/apt/sources.list.d/docker.list'
[ -e $list_file ] && return 8;

output '添加Docker的官方GPG密钥'
gpg_path='/usr/share/keyrings/docker-archive-keyring.gpg'
sudo rm -f $gpg_path
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o $gpg_path

echo "deb [arch=$(dpkg --print-architecture) signed-by=$gpg_path] https://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/ubuntu $(lsb_release -cs) stable" | sudo tee $list_file

sudo apt update