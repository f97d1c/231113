[[ ! $(physical_host?) -eq 0 ]] && return 8;

[[ $(last_change_ago_m '/etc/docker/daemon.json') -gt 10 ]] && return 8;

sudo systemctl daemon-reload
[[ $? -ne 0 ]] && output "重启失败: 容器守护程序重启失败,新环境安装需要重启系统(重启系统当前用户加入Docker组才能生效)." && return 233;

sudo systemctl restart docker