[[ ! $(physical_host?) -eq 0 ]] && return 8;

[[ ! "$(cat /etc/group)" =~ "docker" ]] && sudo groupadd docker;

[[ "$(groups $USER)" =~ "docker" ]] && return 8;

output '添加用户: 当前用户加入Docker组(以非Root用户身份管理Docker)'
sudo usermod -aG docker $USER