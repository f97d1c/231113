[[ ! $(physical_host?) -eq 0 ]] && return 8;

file_path='/etc/docker/daemon.json'
[ -f $file_path ] && return 8;

sudo cp $(get_path_by_name 'daemon.json') $file_path