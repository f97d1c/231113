[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ $(installed? 'docker') -eq 0 ]] && return 8;
sudo apt install -y docker-ce docker-ce-cli containerd.io