[[ ! "${1-''}" =~ '构建镜像' ]] && return 8;
[[ "${2-''}" =~ '多架构' ]] && return 8;

# 只有当参数中包含发布时执行
[[ ! "$@" =~ '发布' ]] && return 8;

docker push $depository_name:$image_tag
code=$?; [ $code -ne 0 ] && output '发布过程异常' && exit 233

return 0