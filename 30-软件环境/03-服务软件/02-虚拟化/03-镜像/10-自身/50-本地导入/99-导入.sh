[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! $(installed? 'docker') -eq 0 ]] && return 233;

# docker pull $image_dep_tag

for line in $(cat $(get_path_by_name '可用镜像地址列表.var')); do
  file_name=${line##*/}
  pure_file_name=${file_name%.*}
  [ $(docker images | grep $pure_file_name 1>/dev/null 2>/dev/null; echo $?) -eq 0 ] && continue

  [[ ! -f "$line" ]] && output '文件不存在: '$line && return 233;
  docker load --input "$line"
done