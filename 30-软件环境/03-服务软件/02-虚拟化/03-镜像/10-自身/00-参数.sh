obj=$(jq '.' $(get_path_by_name '注册镜像.json'))

# 运行时构建容器名称
build_container_name='docker_run_build'

# 仓库名称
depository_name='ff4c00/linux'

# 适配平台架构
platform='linux/amd64,linux/arm64,linux/arm/v7'

# 系统版本
# image_tag_key='ubuntu-18.04'
image_tag_key='ubuntu-22.04'
value=$(jq -r --arg key $image_tag_key '.["\($key)"]' <<<$obj)
# 基础镜像
base_image="$(jq -r '.["仓库"]' <<<$value)@$(jq -r '.["签名"]' <<<$value)"

# 镜像标签
image_tag="${image_tag_key}-$(date +%Y%m%d)"

# 镜像仓库及标签
image_dep_tag="$depository_name:$image_tag"

# 用户名
user_name='ubuntu'

# 用户组名
group_name='ubuntu'
