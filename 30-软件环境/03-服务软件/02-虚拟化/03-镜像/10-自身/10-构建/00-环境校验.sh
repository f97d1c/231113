[[ ! "$1" =~ '构建镜像' ]] && return 8;
[[ ! $(physical_host?) -eq 0 ]] && output '当前环境非宿主机' && exit 233;
[[ ! $(installed? 'docker') -eq 0 ]] && output '尚未安装Docker'  && exit 233;

return 0