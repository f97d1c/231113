[[ ! "$1" =~ '构建镜像' ]] && return 8;
[[ ! "$@" =~ '发布' ]] && return 8;

docker login
if [[ ! $? -eq 0 ]]; then
  output 'DockerHub登录失败, 请尝试手动执行: docker login'
  exit 233
fi

return 0