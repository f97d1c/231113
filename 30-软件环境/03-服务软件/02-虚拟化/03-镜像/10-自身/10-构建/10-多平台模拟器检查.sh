[[ ! "$1" =~ '构建镜像' ]] && return 8;
[[ ! "${2-''}" =~ '多架构' ]] && return 8;
return 8

output '安装Buildkit'
docker pull moby/buildkit
if [[ ! $? -eq 0 ]];then exit 233;fi

output '安装并启动多平台模拟器(全部)'
docker run --privileged --rm tonistiigi/binfmt --install all
if [[ ! $? -eq 0 ]];then exit 233;fi

output '创建基于docker-container驱动的builder实例 local_builder'

docker buildx create --name local_builder --driver docker-container
if [[ ! $? -eq 0 ]];then exit 233;fi

output '切换至local_builder'
docker buildx use local_builder
if [[ ! $? -eq 0 ]];then exit 233;fi

return 0