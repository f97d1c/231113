# 优点在于可以在构建失败后继续执行以节省构建时间
# 与Dockerfile构建相比在保持构建透明性的前提下缩短了构建时间

[[ ! "$@" =~ '构建镜像' ]] && return 8;
[[ ! "$@" =~ '运行时' ]] && return 8;
[[ "$@" =~ '多架构' ]] && return 8;

[ $(docker images | grep $image_tag 1>/dev/null 2>/dev/null; echo $?) -eq 0 ] && return 8

if [[ $(docker ps | grep $build_container_name 1>/dev/null 2>/dev/null; echo $?) -ne 0 ]];then
  output "创建容器: 用于执行构建命令"
  docker run -itd --name $build_container_name $base_image
  [[ $? -ne 0 ]] && return 233;
fi

output "构建镜像: 更新软件源"
docker exec -i $build_container_name apt update 
[[ $? -ne 0 ]] && return 255;

output "构建镜像: 必要依赖安装"
docker exec -i $build_container_name apt install -y sudo curl
[[ $? -ne 0 ]] && return 255;

output "构建镜像: 主体构建"
docker exec -i $build_container_name bash -l -c "curl -sL https://gitlab.com/f97d1c/231113/-/raw/master/curl.sh | bash"
[[ $? -ne 0 ]] && return 255;

docker stop $build_container_name 
[[ $? -ne 0 ]] && return 233;

docker commit $build_container_name $depository_name:$image_tag
[[ $? -ne 0 ]] && return 233;

docker rm $build_container_name 
[[ $? -ne 0 ]] && return 233;

# 运行完成后直接退出 不再执行后续流程 无论返回0或8都如此
return 0