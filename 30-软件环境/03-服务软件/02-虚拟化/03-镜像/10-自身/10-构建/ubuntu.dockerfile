# 指定基础镜像信息
ARG BASE_IMAGE

FROM $BASE_IMAGE

# FROM前声明的ARG在FROM后无法读取
ARG USER_NAME 

# 编写维护者信息
LABEL \
author="mike" \
email="ff4c00@gmail.com"

# 设定默认shell为bash并为交互模式运行
SHELL ["/bin/bash","-ic"]

## 设置环境变量
### 设置系统基本信息 C.UTF-8支持中文
ENV LANG=C.UTF-8

# 系统初始化相关操作
RUN apt update && apt install -y sudo curl
RUN curl -sL https://gitlab.com/f97d1c/231113/-/raw/master/curl.sh | bash

## 设定进入容器后所使用用户
USER $USER_NAME:1000

## 设定进入容器后的默认目录
WORKDIR "/home/$USER_NAME"

# CMD ["source /home/${OPERATOR_USER-$(whoami)}/.bashrc"]
