[[ ! "${1-''}" =~ '构建镜像' ]] && return 8;
[[ ! "${2-''}" =~ '多架构' ]] && return 8;
[[ "$@" =~ '运行时' ]] && return 8;

return 8

sudo docker buildx build \
  -t $depository_name:$image_tag \
  -f $(get_path_by_name "ubuntu.dockerfile") \
  --build-arg BASE_IMAGE=$base_image \
  --build-arg USER_NAME=$user_name \
  --platform $platform
  --push .

code=$?; [ $code -ne 0 ] && output '构建过程异常' && exit 233

return 0