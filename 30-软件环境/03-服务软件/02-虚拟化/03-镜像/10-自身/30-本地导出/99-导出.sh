[[ ! "$1" =~ '构建镜像' ]] && return 8;
[[ "$@" =~ '多架构' ]] && return 8;

store_path=$(get_value '本地镜像导出地址.var')
[[ ! -d "$store_path" ]] && output '[本地镜像导出地址]不存在' && return 233;

store_dir="$store_path/$(uname -m)/$depository_name"
[[ ! -d "$store_dir" ]] && mkdir -p "$store_dir"

store_path="$store_dir/${image_dep_tag##*:}.tar"
[[ -f "$store_path" ]] && return 8;

[ $(docker images | grep ${image_dep_tag##*:} 1>/dev/null 2>/dev/null; echo $?) -ne 0 ] && \
output "镜像不存在: $image_dep_tag" && \
return 233

sed -i "/$image_tag_key/d" $(get_path_by_name '可用镜像地址列表.var')
tee -a $(get_path_by_name '可用镜像地址列表.var') <<EOF
$store_path
EOF

tee $(get_path_by_name 'IMAGE_SELF_MAIN_DEP_TAG.export') <<EOF
$image_dep_tag
EOF

docker save $depository_name:${image_dep_tag##*:} > $store_path
code=$?; [ $code -ne 0 ] && output '导出过程异常' && exit 233

return 0