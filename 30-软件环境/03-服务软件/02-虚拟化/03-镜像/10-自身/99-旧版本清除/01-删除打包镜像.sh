[[ ! $(physical_host?) -eq 0 ]] && return 8;

for line in $(cat $(get_path_by_name '可用镜像地址列表.var')); do
  file_name=${line##*/}
  pure_file_name=${file_name%.*}

  same_tag_files=($(ls ${line%/*} | grep ${file_name%-*}))
  for tag_version in ${same_tag_files[@]};do 
    [[ "$tag_version" == "$file_name" ]] && continue;
    path="${line%/*}/$tag_version"
    output "历史镜像: 删除 $path"
    rm -f $path
  done

done