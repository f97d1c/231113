[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! $(installed? 'docker') -eq 0 ]] && return 233;

obj=$(jq '.' $(get_path_by_name '注册镜像.json'))
keys=($(jq -c -r '.| keys_unsorted[]' <<<$obj))

for key in "${keys[@]}";do 
  value=$(jq -r --arg key $key '.["\($key)"]' <<<$obj)
  [[ ! $(docker images --digests | grep $(jq '.["签名"]' <<<$value) >/dev/null; echo $?) -eq 0 ]] && continue;
  output "拉取镜像: $key"
  docker pull "$(jq -r '.["仓库"]' <<<$value)@$(jq -r '.["签名"]' <<<$value)"
done