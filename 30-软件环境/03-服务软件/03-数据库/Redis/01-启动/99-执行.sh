[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "control.node" ]] && return 8;
[[ $(docker ps | grep redis >/dev/null; echo $?) -eq 0 ]] && return 8;

docker-compose --compatibility -f $(get_path_by_name 'Redis-latest.yaml') up -d