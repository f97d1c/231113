[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "control.node" ]] && return 8;
[[ $(docker ps | grep oracle-11g-xe >/dev/null; echo $?) -eq 0 ]] && return 8;

docker-compose -f $(get_path_by_name 'oracle-11g-xe.yaml') up -d