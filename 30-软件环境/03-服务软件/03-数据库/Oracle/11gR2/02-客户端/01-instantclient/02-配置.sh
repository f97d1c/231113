[[ $(physical_host?) -eq 0 ]] && return 8;
[[ -d "/opt/oracle/$file_name" ]] && return 8;

sudo mkdir -p /opt/oracle

sudo unzip -n $store_path -d /opt/oracle

sudo ln -s "/opt/oracle/$file_name/libclntsh.so.12.1" "/opt/oracle/$file_name/libclntsh.so"