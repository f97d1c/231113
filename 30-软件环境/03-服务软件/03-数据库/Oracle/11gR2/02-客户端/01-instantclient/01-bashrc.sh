[[ $(physical_host?) -eq 0 ]] && return 8;

tee -a "$HOME/.bashrc"<<-EOF

# === Oracle相关追加内容开始 ===
export LD_LIBRARY_PATH=/opt/oracle/$file_name
export NLS_LANG=AMERICAN_AMERICA.UTF8 # 汉字显示乱码
# === Oracle相关追加内容结束 ===

EOF
