[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "control.node" ]] && return 8;
[[ $(docker ps | grep mysql >/dev/null; echo $?) -eq 0 ]] && return 8;

docker-compose --compatibility -f $(get_path_by_name 'mysql-5.7.yaml') up -d