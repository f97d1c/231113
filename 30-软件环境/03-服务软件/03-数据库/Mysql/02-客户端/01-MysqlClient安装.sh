[[ $(physical_host?) -eq 0 ]] && return 8;

if [[ "$(lsb_release -a)" =~ '18.04' ]];then
  [[ $(installed? "mysql-client") -ne 0 ]] && sudo apt install -y mysql-client;
  [[ $(installed? "libmysqlclient-dev") -ne 0 ]] && sudo apt install -y libmysqlclient-dev;
  [[ $(installed? "libssl-dev") -ne 0 ]] && sudo apt install -y libssl-dev;
else
  # mysql-client的开源版本(开源的MySQL现在叫MariaDB)
  [[ $(installed? "mariadb-client") -ne 0 ]] && sudo apt install -y mariadb-client;
  # 指向libmariadb-dev库的符号链接
  [[ $(installed? "libmariadb-dev-compat") -ne 0 ]] && sudo apt install -y libmariadb-dev-compat;
  [[ $(installed? "libssl-dev") -ne 0 ]] && sudo apt install -y libssl-dev;
fi

return 0
