[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "vpn.relay.node" ]] && return 8;
[[ ! "$(hostname)" =~ "control.node" ]] && return 8;
[[ "$(pgyvisitor getmbrs)" =~ "login" ]] && return 8;

# 获取本机虚拟ip地址
text=$(pgyvisitor getmbrs -m | grep -A 1 '本机' | tail -n 1)
virtually_ip_v4=$(echo $text | sed 's/.*://')
pattern='([[:digit:]]{1,3}\.){3}[[:digit:]]{1,3}' 
[[ ! $virtually_ip_v4 =~ $pattern ]] && echo '虚拟IP格式有误' && return 233;

echo "${NFS_SERVER_ADRESS[@]}" | grep -qw "$virtually_ip_v4" >/dev/null; 
[[ $? -eq 0 ]] && return 8;

fsv_array_item_add 'NFS_SERVER_ADRESS.export' $virtually_ip_v4