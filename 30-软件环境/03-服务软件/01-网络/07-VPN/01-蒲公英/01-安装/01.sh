[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! $(installed? 'pgyvisitor') -eq 0 ]] && return 8;

# 这个网址自动下载最新版本: https://pgy.oray.com/softwares/153/download/2156/
# wget https://pgy.oray.com/softwares/153/download/2156/PgyVisitor_6.2.0_x86_64.deb
# dpkg -i PgyVisitor_6.2.0_x86_64.deb