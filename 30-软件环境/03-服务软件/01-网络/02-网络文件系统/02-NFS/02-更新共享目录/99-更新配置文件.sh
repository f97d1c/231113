[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "control.node" ]] && return 8;

conf_path='/etc/exports'
check_md5 $(get_path_by_name 'NFS共享目录配置.cnf') $conf_path
[[ $? -eq 0 ]] && return 8;

backup_file $conf_path

sudo cp $(get_path_by_name 'NFS共享目录配置.cnf') $conf_path

sudo exportfs -rv