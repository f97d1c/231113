[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "control.node" ]] && return 8;

echo "${NFS_SERVER_ADRESS[@]}" | grep -qw "$IP_LOCAL" >/dev/null; 
[[ $? -eq 0 ]] && return 8;

# systemd执行总是为空
[ "$IP_LOCAL" == "" ] && return 8;

fsv_array_item_add 'NFS_SERVER_ADRESS.export' $IP_LOCAL