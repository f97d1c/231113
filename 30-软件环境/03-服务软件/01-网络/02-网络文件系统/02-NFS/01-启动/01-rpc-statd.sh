[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "control.node" ]] && return 8;

[ $(systemctl status rpc-statd >/dev/null; echo $?) -eq 0 ] && return 8;

sudo systemctl start rpc-statd