[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "control.node" ]] && return 8;
[ ! -f "/etc/init.d/nfs-kernel-server" ] && return 233;

[ $(systemctl status nfs-kernel-server >/dev/null; echo $?) -eq 0 ] && return 8;

sudo /etc/init.d/nfs-kernel-server start