[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "master.control.node" ]] && return 8;
[[ "$MASTER_NFS_SERVER_ADRESS" == "$IP_LOCAL" ]] && return 8;

# systemd执行总是为空
[ "$IP_LOCAL" == "" ] && return 8;

set_value 'MASTER_NFS_SERVER_ADRESS.export' $IP_LOCAL