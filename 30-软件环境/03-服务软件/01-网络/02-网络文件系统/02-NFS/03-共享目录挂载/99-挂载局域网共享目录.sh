[[ ! $(physical_host?) -eq 0 ]] && return 8;

mount_path="/home/$USER/nfs"
[[ "$(mount | grep nfs)" =~ $mount_path ]] && return 8;
[ ! -d "$mount_path" ] && mkdir -p $mount_path

# 防止变量值在前面更新
eval "server_ips=$(get_value 'NFS_SERVER_ADRESS.export')"

for server_ip in "${server_ips[@]}"; do
  output "开始检查: $server_ip"

  ping -c 1 -w 1 $server_ip > /dev/null
  [ $? -ne 0 ] && output "无法连接: $server_ip" && continue

  sudo showmount -e $server_ip > /dev/null
  [ $? -ne 0 ] && output "无法挂载: $server_ip" && continue

  sudo mount -vvvv -t nfs -o vers=4 $server_ip:/home/ubuntu/mnt/share $mount_path
  [[ $? -eq 0 ]] && break
done

[[ ! "$(mount | grep nfs)" =~ $mount_path ]] && output '未找到可用共享地址' && return 233;