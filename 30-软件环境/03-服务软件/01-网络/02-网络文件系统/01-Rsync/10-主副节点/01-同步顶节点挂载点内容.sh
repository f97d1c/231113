[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "deputy.control.node" ]] && return 8;
[[ "$MASTER_NFS_SERVER_ADRESS" == "$IP_LOCAL" ]] && return 8;

# 同步等待时间过长放入后台执行
[[ $(jobs -l | grep 'rsync' > /dev/null ; echo $?) -eq 0 ]] && return 8;

rsync_log_path="$PWD/.rsync.log"
if [ -f $rsync_log_path ]; then
  if [[ $(cat $rsync_log_path | grep 'rsync error' > /dev/null 2>&1; echo $?) -eq 0 ]];then
    cat $rsync_log_path
    output "上次远程同步失败"
    return 233
  fi
fi

if [[ $(last_change_ago_m $rsync_log_path) -lt 10 ]];then
  output "距离上次更新时间间隔小于10分钟"
  return 8
fi

# 目前顶节点和主节点为同一机器, 目前尚未编写针对主节点的同步逻辑
# mysql数据被挂载为root所有 所以需要sudo
# sudo rsync -arvzhP --rsync-path="sudo rsync" -e "ssh -i /home/$USER/.ssh/id_rsa" --delete ubuntu@$MASTER_NFS_SERVER_ADRESS:$MASTER_NFS_TARGET_PATH/ /home/$USER/mnt > $rsync_log_path 2>&1 &
cmd="sudo rsync -arvzhP --verbose --rsync-path='sudo rsync' -e 'ssh -i /home/$USER/.ssh/id_rsa' --delete ubuntu@$MASTER_NFS_SERVER_ADRESS:$MASTER_NFS_TARGET_PATH/ /home/$USER/mnt >> $rsync_log_path 2>&1"

echo "[$(date '+%Y-%m-%dT%H:%M:%S.%3N')]: $cmd" > $rsync_log_path
eval "$cmd" &