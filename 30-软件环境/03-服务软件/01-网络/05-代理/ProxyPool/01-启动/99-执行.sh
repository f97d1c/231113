[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "control.node" ]] && return 8;
[[ $(installed? 'docker') -ne 0 ]] && return 8;
[[ $(docker ps 1>/dev/null 2>/dev/null; echo $?) -ne 0 ]] && return 8;

[[ $(docker ps | grep proxy_pool >/dev/null; echo $?) -eq 0 ]] && return 8;

[[ $(docker ps | grep redis >/dev/null; echo $?) -ne 0 ]] && docker-compose -f $(get_path_by_name 'Redis-latest.yaml') up -d

docker-compose -f $(get_path_by_name 'ProxyPool-latest.yaml') up -d