[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "main.control.node" ]] && return 8;
[[ ! -f "$pub_file" ]] && return 233

md5_value=$(cat "$pub_file" | md5sum)
md5_value=${md5_value:0:32}

store_file="$pub_store_dir/$md5_value"

[[ -f "$store_file" ]] && return 8;

cat "$pub_file" > "$store_file"