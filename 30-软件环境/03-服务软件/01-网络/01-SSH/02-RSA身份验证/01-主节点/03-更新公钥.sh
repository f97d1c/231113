[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "main.control.node" ]] && return 8;

for file in "$pub_store_dir/*";do
  content=$(cat $file) 
  grep "$content" $authorized_keys 1>/dev/null 2>/dev/null;
  [[ $? -eq 0 ]] && continue;
  echo $content | tee -a "$authorized_keys"
done