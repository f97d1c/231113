[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ "$(hostname)" =~ "main.control.node" ]] && return 8;

[[ ! -d "$ssh_path" ]] && mkdir -p "$ssh_path"

cat /dev/null > $authorized_keys