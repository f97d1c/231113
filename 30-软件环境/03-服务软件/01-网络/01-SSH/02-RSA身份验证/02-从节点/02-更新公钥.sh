[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ "$(hostname)" =~ "main.control.node" ]] && return 8;

for file in "$pub_store_dir/*";do
  cat $file | tee -a "$authorized_keys"
done