[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(ps -e | grep ssh)" == "" ]] && return 8;

output "服务端启动"
sudo service ssh start

output "服务端配置自启动"
sudo systemctl enable ssh