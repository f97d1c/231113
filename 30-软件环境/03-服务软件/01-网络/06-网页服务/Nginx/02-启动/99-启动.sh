[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "control.node" ]] && return 8;
[[ $(installed? 'docker') -ne 0 ]] && return 8;
[[ $(docker ps 1>/dev/null 2>/dev/null; echo $?) -ne 0 ]] && return 8;

[[ $(docker ps | grep nginx 1>/dev/null 2>/dev/null; echo $?) -eq 0 ]] && return 8;

docker-compose -f $(get_path_by_name 'DC.Nginx.yml') up -d