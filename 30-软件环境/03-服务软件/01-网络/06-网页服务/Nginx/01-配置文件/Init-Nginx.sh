#!/bin/bash --login

function installed? (){
  which "$1" 1>/dev/null 2>/dev/null
  [[ $? -eq 0 ]] && echo 0 && return;

  dpkg -l | grep " $1 " 1>/dev/null 2>/dev/null
  [[ $? -eq 0 ]] && echo 0 && return;

  echo 255
}

# dpkg --configure -a

[[ $(installed? 'nginx-extras') -ne 0 ]] && apt update && echo 'N' | apt-get install -y nginx-extras
[[ $(installed? 'git') -ne 0 ]] && apt update && apt-get install -y git
[[ $(installed? 'wget') -ne 0 ]] && apt update && apt-get install -y wget
[[ $(installed? 'zip') -ne 0 ]] && apt update && apt-get install -y zip
# [[ $(installed? 'cmark') -ne 0 ]] && apt update && apt-get install -y cmark

if [[ ! -d '/usr/share/nginx/themes/modern' ]];then
  echo '模块安装: FancyIndex目录索引'

  mkdir -p '/usr/share/nginx/themes'
  wget -c -t 100 https://github.com/gonace/nginx-fancyindex-modern-theme/releases/download/1.0.2/modern-1.0.2.zip
  unzip modern-1.0.2.zip -d /usr/share/nginx/themes/
fi

# 编译 Nginx 需下载 Nginx 源码并编译安装
# if [[ ! -d '/ngx_markdown_filter_module' ]];then
#   echo '模块安装: Markdown渲染'
#   git clone https://github.com/ukarim/ngx_markdown_filter_module.git
#   cd /usr/share/nginx
#   ./configure --add-module=/ngx_markdown_filter_module
#   make
#   cd -
# fi

# 启动 Nginx 服务器,并将其设置为不以守护进程模式运行
nginx -g 'daemon off;'