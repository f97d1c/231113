[[ $(physical_host?) -eq 0 ]] && return 8;
[[ $(installed? "r-base") -eq 0 ]] && return 8;

# 参考资料: https://cloud.r-project.org/bin/linux/ubuntu/

# update indices
sudo apt update -qq
# install two helper packages we need
sudo apt install --no-install-recommends software-properties-common dirmngr
# add the signing key (by Michael Rutter) for these repos
# To verify key, run gpg --show-keys /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc 
# Fingerprint: E298A3A825C0D65DFD57CBB651716619E084DAB9
wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | sudo tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc
# add the R 4.0 repo from CRAN -- adjust 'focal' to 'groovy' or 'bionic' as needed
sudo add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/"

sudo apt install --no-install-recommends -y r-base

sudo chown -R $USER /usr/local/lib/R/site-library

sudo add-apt-repository -y ppa:c2d4u.team/c2d4u4.0+
sudo apt update
sudo apt install --no-install-recommends -y r-cran-tidyverse