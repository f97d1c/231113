if (!require('rjson', character.only = TRUE)) {
  install.packages('rjson', repos = "https://mirrors.ustc.edu.cn/CRAN/")
}
library(rjson)

RootPath = getwd()

requires = fromJSON(file=file.path(RootPath, '30-软件环境/05-开发软件/01-脚本语言/05-R/02-插件安装/R相关插件.json'))

for (key in names(requires)) {
  current_time = format(Sys.time(), "[%Y-%m-%d %H:%M:%OS3]")
  cat(current_time, '')

  if (!require(key, character.only = TRUE)) {
    install.packages(key, repos = "https://mirrors.ustc.edu.cn/CRAN/")
  }

  if (!is.loaded(key)) {
    library(key, character.only = TRUE)
  }
}