[[ $(physical_host?) -eq 0 ]] && return 8;
[[ $(installed? "rvm") -eq 0 ]] && return 8;

# https://rvm.io/rvm/offline

make_rvm_path="$HOME/rvm"
[[ ! -d "$make_rvm_path" ]] && mkdir "$make_rvm_path" 

path_=$PWD

package_path="$PWD/$(get_path_by_name 'rvm-stable.tar.gz')"

cd "$make_rvm_path"

tar --strip-components=1 -xzf $package_path

./install --auto-dotfiles

source $source_rvm_path

cd $path_