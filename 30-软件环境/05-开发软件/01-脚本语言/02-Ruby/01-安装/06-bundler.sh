[[ $(physical_host?) -eq 0 ]] && return 8;
[[ $(installed? "bundle") -eq 0 ]] && return 8;

if [[ "$bundler_version" ]];then
  gem install bundler -v $bundler_version
else
  gem install bundler
fi