[[ $(physical_host?) -eq 0 ]] && return 8;
[[ $(installed? "rvm") -eq 0 ]] && return 8;

gpg2 --keyserver keyserver.ubuntu.com --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
# export PATH="$PATH:$HOME/.rvm/bin"

curl -sSL https://get.rvm.io | bash -s stable
[[ -f "$source_rvm_path" ]] && source $source_rvm_path
return 0