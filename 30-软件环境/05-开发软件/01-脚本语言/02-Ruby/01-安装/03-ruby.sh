[[ $(physical_host?) -eq 0 ]] && return 8;
[[ $(installed? "rvm") -ne 0 ]] && return 233;
[[ $(rvm list | grep $ruby_version >/dev/null; echo $?) -eq 0 ]] && return 8;

if [[ "$(lsb_release -a)" =~ '22.04' ]];then
  # 22.04 默认附带最新版本的 OpenSSL,但与旧版本的 Ruby 不兼容
  rvm pkg install openssl
  rvm install $ruby_version --with-openssl-dir=$HOME/.rvm/usr
else
  rvm install $ruby_version
fi

rvm use $ruby_version --default