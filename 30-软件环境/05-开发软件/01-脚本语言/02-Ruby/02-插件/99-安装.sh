[[ $(physical_host?) -eq 0 ]] && return 8;
[[ ! $(installed? "ruby") -eq 0 ]] && return 255;
[[ ! "$(ruby -v)" =~ "$ruby_version" ]] && return 8;

gems=$(jq '.' $(get_path_by_name '常用GEM清单.json'))

keys=($(jq -c -r '.| keys_unsorted[]' <<<$gems))
for key in "${keys[@]}";do 
  [[ $(gem list | grep $key >/dev/null; echo $?) -eq 0 ]] && continue;

  detail=$(jq -r --arg key $key '.["\($key)"]' <<<$gems)
  output "开始安装: $key, 用于: $(jq -r '.["说明"]' <<<$detail)."
  gem_version=$(jq -r '.["版本"]//""' <<<$detail)
  if [[ -n $gem_version ]];then
    gem install $key -v $gem_version
  else
    gem install $key
  fi
  code=$?
  [[ ! $code -eq 0 ]] && return $code;
done