source_rvm_path="$HOME/.rvm/scripts/rvm"
[[ -f "$source_rvm_path" ]] && source $source_rvm_path

# ruby版本
# 低版本依赖libssl1.0-dev在20及更高版本不再支持
ruby_version=2.7.2

# bundler版本
# bundler_version=2.3.27