[[ $(physical_host?) -eq 0 ]] && return 8;
[[ ! $(installed? "pip") -eq 0 ]] && return 255;

pip install --upgrade pip