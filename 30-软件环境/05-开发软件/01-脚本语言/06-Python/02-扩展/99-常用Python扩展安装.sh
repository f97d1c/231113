[[ $(physical_host?) -eq 0 ]] && return 8;

contents=$(jq '.' $(get_path_by_name '常用Python扩展.json'))

keys=($(jq -c -r '.| keys_unsorted[]' <<<$contents))
for key in "${keys[@]}";do 
  output "开始检查: $key."
  [[ $(pip list | grep $key >/dev/null; echo $?) -eq 0 ]] && continue;

  detail=$(jq -r --arg key $key '.["\($key)"]' <<<$contents)
  output "开始安装: $key, 用于: $(jq -r '.["说明"]' <<<$detail)."
  version=$(jq -r '.["版本"]' <<<$detail)
  
  if [[ "$version" != 'null' ]];then
    pip install "$key==$version"
  else
    pip install $key
  fi

  code=$?
  [[ ! $code -eq 0 ]] && return $code;
done

return 0