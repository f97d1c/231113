[[ $(physical_host?) -eq 0 ]] && return 8;

current_python_version=$(python -c 'import sys; version=sys.version_info[:3]; print("{0}.{1}.{2}".format(*version))')
pip_python_version=$(pip -V | sed -E 's/.*versions\/([0-9]\.[0-9]\.[0-9])\/envs.*/\1/g')
[ "$pip_python_version" == "$current_python_version" ] && return 8;

python -m pip install --upgrade pip

current_python_version=$(python -c 'import sys; version=sys.version_info[:3]; print("{0}.{1}.{2}".format(*version))')
pip_python_version=$(pip -V | sed -E 's/.*versions\/([0-9]\.[0-9]\.[0-9])\/envs.*/\1/g')
if [ ! "$pip_python_version" == "$current_python_version" ]; then
  output "pip使用版本与当前Python版本不符"
  return 255
fi