[[ $(physical_host?) -eq 0 ]] && return 8;
[ -n "$(pyenv virtualenvs | grep $virtualenv_name)" ] && return 8;

pyenv virtualenv $python_version $virtualenv_name
[[ ! $? -eq 0 ]] && output "虚拟环境构建不符预期" && return 255;

pyenv activate $virtualenv_name
[[ ! $? -eq 0 ]] && output "虚拟环境切换失败" && return 255;

current_python_version=$(python -c 'import sys; version=sys.version_info[:3]; print("{0}.{1}.{2}".format(*version))')
if [ ! "$python_version" == "$current_python_version" ]; then
  output "当前默认Python版本: $current_python_version"
  output '[false, "Python版本不符预期"]'
  return 255
fi