[[ $(physical_host?) -eq 0 ]] && return 8;
[ ! -d $pyenv_install_path ] && return 255;
[ -d "$pyenv_install_path/plugins/pyenv-virtualenv" ] && return 8;

# current_path=$PWD
# cd $pyenv_install_path/plugins
# git clone https://github.com/pyenv/pyenv-virtualenv.git
# cd $current_path

unzip -n $(get_path_by_name '20240123-pyenv-virtualenv.zip')
mv pyenv-virtualenv-master $pyenv_install_path/plugins/pyenv-virtualenv