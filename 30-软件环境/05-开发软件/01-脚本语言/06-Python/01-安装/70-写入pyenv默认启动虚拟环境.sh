[[ $(physical_host?) -eq 0 ]] && return 8;
[[ "$(cat /home/$USER/.bashrc)" =~ "pyenv默认启动虚拟环境" ]] && return 8;

pyenv activate $virtualenv_name

tee -a "/home/$USER/.bashrc" <<-EOF

# === pyenv默认启动虚拟环境开始 ===
# 不再在命令行前显示环境名称
export PYENV_VIRTUALENV_DISABLE_PROMPT=1
[[ ! "占位符--当前Python版本" =~ "$python_version" ]] && pyenv activate $virtualenv_name
# === pyenv默认启动虚拟环境结束 ==="
EOF

sed -i -e 's/占位符--当前Python版本/$(python --version 2>\/dev\/null)/g' "/home/$USER/.bashrc"