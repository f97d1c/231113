[[ $(physical_host?) -eq 0 ]] && return 8;
[ -d $pyenv_install_path ] && return 8;

# Github长时间无法访问
# git clone https://github.com/pyenv/pyenv.git $pyenv_install_path

unzip -n $(get_path_by_name '20240123-pyenv.zip')
mv pyenv-master $pyenv_install_path