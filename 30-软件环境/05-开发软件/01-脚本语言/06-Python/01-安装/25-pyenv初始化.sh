[[ $(physical_host?) -eq 0 ]] && return 8;
[[ $(installed? "pyenv") -eq 0 ]] && return 8;

[ ! -d $pyenv_install_path ] && return 255;
[ ! -d "$pyenv_install_path/plugins/pyenv-virtualenv" ] && return 255;

export PATH="/home/$USER/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
export PATH="/root/.pyenv/shims:${PATH}"
eval "$(pyenv init --path)"