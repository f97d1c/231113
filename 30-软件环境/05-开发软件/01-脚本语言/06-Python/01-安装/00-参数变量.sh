# Python版本
python_version='3.9.8'

# pyenv安装位置
pyenv_install_path="/home/$USER/.pyenv"

# 虚拟环境名称
virtualenv_name="python_$python_version"