[[ $(physical_host?) -eq 0 ]] && return 8;
# version_install_path="$pyenv_install_path/versions/$python_version"
version_install_path="$(pyenv root)/versions/$python_version"
[ -d $version_install_path ] && return 8;

pyenv install $python_version