[[ $(physical_host?) -eq 0 ]] && return 8;
[[ "$(cat /home/$USER/.bashrc)" =~ "pyenv相关内容" ]] && return 8;

sudo tee -a /home/$USER/.bashrc <<-'EOF'

# === pyenv相关内容开始 ===
export PATH="/home/$USER/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
export PATH="/root/.pyenv/shims:${PATH}"
EOF

echo -e "$(pyenv init --path)" >>/home/$USER/.bashrc
echo -e "# === pyenv相关内容结束 ===" >>/home/$USER/.bashrc