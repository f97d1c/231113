[[ $(physical_host?) -eq 0 ]] && return 8;
[[ $(installed? "node") -eq 0 ]] && [[ $(node -v | sed -E "s/v([0-9]+).*/\1/g") -gt 13 ]] && return 8;

# [NodeSource Node.js Binary Distributions](https://github.com/nodesource/distributions)

if [[ "$(lsb_release -a)" =~ '18.04' ]];then
  node_version=17
else
  node_version=18
fi

curl -fsSL https://deb.nodesource.com/setup_$node_version.x | sudo bash
sudo apt install -y nodejs

output '设置npm国内更新源'
npm config set registry=https://registry.npmmirror.com

# ouput '安装cnpm'
# npm install -g cnpm --registry=https://registry.npmmirror.com

[[ $(node -v | sed -E "s/v([0-9]+).*/\1/g") -lt 13 ]] && return 255;