find -name '*.json' -type f -print0 | while IFS= read -r -d $'\0' file; do 
  output "检查文件: $file"

  content=$(jq -e '.["相关依赖"]' $file)
  [[ ! $? -eq 0 ]] && continue;

  keys=($(jq -c -r '.| keys_unsorted[]' <<<$content))
  for key in "${keys[@]}";do 
    output '开始检查: '$key
    detail=$(jq -r --arg key $key '.["\($key)"]' <<<$content)

    if [[ $(jq -r '.["仅宿主环境"]' <<<$detail) == 'true' ]];then
      [[ "$(ps -p 1 -o comm=)" != 'systemd' ]] && continue;
    fi
    
    if [[ $(jq -r '.["仅虚拟环境"]' <<<$detail) == 'true' ]];then
      [[ "$(ps -p 1 -o comm=)" == 'systemd' ]] && continue;
    fi

    [[ $(installed? "$key") -eq 0 ]] && continue;
    output '开始安装: '$key
    sudo apt install -y "$key"
    code=$?
    [[ ! $code -eq 0 ]] && return $code;
  done
done