[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "main.control.node" ]] && return 8;
[[ $(installed? 'qbittorrent-nox') -ne 0 ]] && return 8;
[[ $(port_used? '9595') -eq 0 ]] && return 8;

qbittorrent-nox -d --webui-port=9595