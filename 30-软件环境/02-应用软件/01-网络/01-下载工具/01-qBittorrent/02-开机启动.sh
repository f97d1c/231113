[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "main.control.node" ]] && return 8;
[[ $(installed? 'qbittorrent-nox') -ne 0 ]] && return 8;
return 8;

sudo tee /etc/systemd/system/qbittorrent-nox.service <<-EOF
[Unit]
Description=qBittorrent client
After=network.target

[Service]
# 默认用户名: admin,密码: adminadmin
ExecStart=/usr/bin/qbittorrent-nox --webui-port=9595
Restart=always

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl enable qbittorrent-nox