[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ ! "$(hostname)" =~ "main.control.node" ]] && return 8;
[[ $(installed? 'qbittorrent-nox') -eq 0 ]] && return 8;

# sudo add-apt-repository --yes ppa:qbittorrent-team/qbittorrent-stable
# sudo apt update

sudo apt install -fy qbittorrent-nox