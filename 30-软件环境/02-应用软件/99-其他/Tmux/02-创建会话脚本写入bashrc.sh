[[ $(physical_host?) -ne 0 ]] && return 8;
[[ ! "$(hostname)" =~ "main.control.node" ]] && return 8;
[[ "$(cat /home/$USER/.bashrc)" =~ "tmux相关内容" ]] && return 8;

sudo tee -a /home/$USER/.bashrc <<-'EOF'

# === tmux相关内容开始 ===
if [[ ! "$TMUX" ]];then
  if [[ $(tmux ls 2>/dev/null | grep 'master' 1>/dev/null 2>/dev/null; echo $?) -ne 0 ]];then
EOF

sudo tee -a /home/$USER/.bashrc <<-EOF
    bash "/home/$USER/init/$(get_path_by_name '创建Tmux主节点会话.sh')"
  fi
fi
# === tmux相关内容结束 ===
EOF