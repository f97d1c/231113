[[ "$TMUX" ]] && output "状态异常: 退出当前会话重试." && return 233;
[[ $(tmux ls 2>/dev/null | grep 'cluster' 1>/dev/null 2>/dev/null; echo $?) -eq 0 ]] && return 8;

tmux new-session -d -s cluster -x "$(tput cols)" -y "$(tput lines)"

tmux split-window -h

tmux selectp -t 0
tmux split-window -v -p 50

tmux selectp -t 0
tmux split-window -v -p 50

tmux selectp -t 2
tmux split-window -v -p 50

tmux selectp -t 4
tmux split-window -v -p 50

tmux send-keys -t cluster:0.0 "ssh 192.168.31.136 -t 'htop'" C-m
tmux send-keys -t cluster:0.1 "ssh 192.168.31.190 -t 'htop'" C-m
tmux send-keys -t cluster:0.2 "htop" C-m
tmux send-keys -t cluster:0.3 "ssh 192.168.31.109 -t 'htop'" C-m
tmux send-keys -t cluster:0.4 "cd && clear && docker attach self.container.system" C-m
tmux send-keys -t cluster:0.5 "cd && cd nfs && clear" C-m