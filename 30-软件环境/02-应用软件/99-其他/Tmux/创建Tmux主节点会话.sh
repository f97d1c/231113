tmux new-session -d -s "master" -x "$(tput cols)" -y "$(tput lines)"

tmux split-window -h

tmux selectp -t 0
tmux split-window -v -p 77

tmux selectp -t 2
tmux split-window -v -p 50

tmux selectp -t 2
tmux split-window -v -p 50

tmux selectp -t 4
tmux split-window -v -p 50

tmux send-keys -t master:0.0 "htop" C-m
tmux send-keys -t master:0.1 "btop" C-m
tmux send-keys -t master:0.2 "cd && clear && docker attach self.container.system" C-m
tmux send-keys -t master:0.3 "cd && clear && echo \"$(systemctl status me-myself-i)\"" C-m
tmux send-keys -t master:0.4 "cd && clear && cd /home/ubuntu/mnt/yggc/app/data_centre &&  docker-compose up -d && docker attach data_centre.python.web" C-m
tmux send-keys -t master:0.5 "cd /home/ubuntu/mnt/share/knowledge/人文科学/经济学/金融/微观金融学/金融体系/策略研究/模拟定投/ && clear" C-m