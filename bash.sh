#!/bin/bash --login
params="$@"

function output (){
  printf "[$(date '+%Y-%m-%dT%H:%M:%S.%3N')] "
  printf "$*"
  printf "\n"
}

function core () {
  find . -type f -name "[0-9][0-9]-*.sh" | sort -n | tr '\n' '\0' | while IFS= read -r -d $'\0' spath; do 
    echo "[$(date '+%Y-%m-%dT%H:%M:%S.%3N')]"' 开始加载: '$spath
    source $spath "$params"
    code=$?
    if [[ $code -eq 8 ]];then output '跳过执行: 逻辑不符合未执行';fi
    ([[ ! $code -eq 0 ]] && [[ ! $code -eq 8 ]]) && echo '[false, "返回值异常", '$code']' && return $code;

    # 不使用if判断而是&&的弊端在于 如果不满足条件则该句的返回状态为1.
    # 所以一定要确保能走到最后的一定要执行或者用if判断.
    # [[ $code -eq 0 ]] && output '执行结果: 成功';
    if [[ $code -eq 0 ]];then output '执行结果: 成功';fi
  done

  code=$?; [[ ! $code -eq 0 ]] && return $code;
  return 0
}

for index in {1..5};do
  core
  code=$?
  [ $code -eq 0 ] && break
  [ $code -eq 233 ] && echo '[false, "需手动介入处理"]' && exit 233
  output "状态异常: $code, 尝试重复执行($index)."
  [ $index -eq 5 ] && echo '[false, "执行失败"]' && exit 255

  # 虚拟环境内双重循环
  [ "$(ps -p 1 -o comm=)" != 'systemd' ] && [ $(( index * 3 )) -gt 5 ] && echo '[false, "执行失败"]' && exit 255
done

echo '[true, ""]'