[[ ! $(physical_host?) -eq 0 ]] && return 8;

nodes=$(jq '.["节点"]' $(get_path_by_name '.机器设备.json'))

keys=($(jq -c -r '.| keys_unsorted[]' <<<$nodes))
for key in "${keys[@]}";do 
  [[ ! $(ip addr | grep $key >/dev/null; echo $?) -eq 0 ]] && continue;
  detail=$(jq -r --arg key $key '.["\($key)"]' <<<$nodes)
  role_names=$(jq -r '.["角色"] | join("-")' <<<$detail)
  [[ "$role_names" == '' ]] && output '角色名称为空' && return 255;
  [[ "$(hostname)" == "$role_names" ]] && continue;

  # 解决问题: sudo: 无法解析主机：$role_names: 未知的名称或服务
  if [[ $(grep $role_names /etc/hosts > /dev/null 2>&1; echo $?) -ne 0 ]];then
    echo "127.0.0.1       $role_names" | sudo tee -a /etc/hosts
  fi

  sudo hostnamectl set-hostname $role_names

done

[[ "$(hostname)" =~ ".node" ]] && return 0;

# 默认为node角色
sudo hostnamectl set-hostname 'job.node'