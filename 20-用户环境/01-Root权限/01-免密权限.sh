#!/bin/bash --login
# 设置为在执行的时候不输入密码

[[ "$(sudo cat /etc/sudoers)" =~ "$USER" ]] && return 8;

output 'soduers文件添加写权限'
sudo chmod u+w /etc/sudoers

sudo tee -a /etc/sudoers <<-EOF
%$USER   ALL=(ALL)   NOPASSWD: ALL
EOF