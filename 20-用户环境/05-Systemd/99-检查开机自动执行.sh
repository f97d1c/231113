[[ ! $(physical_host?) -eq 0 ]] && return 8;
[[ "$(systemctl is-enabled me-myself-i)" == 'enabled' ]] && return 8;

sudo systemctl enable me-myself-i