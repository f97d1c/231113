[[ ! $(physical_host?) -eq 0 ]] && return 8;

service_path='/etc/systemd/system/me-myself-i.service'

if [[ -f "$service_path" ]];then
  check_md5 $(get_path_by_name 'me-myself-i.service') $service_path
  [[ $? -eq 0 ]] && return 8;
fi

sudo cp $(get_path_by_name 'me-myself-i.service') $service_path