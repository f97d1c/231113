
# 说明

## Systemd

Systemd 是 Linux 系统下的一种 init 软件,由 Lennart Poettering 带头开发,<br>
并在 LGPL 2.1 及其后续版本许可证下开源发布.

Systemd 的开发目标是提供更优秀的框架以表示系统服务间的依赖关系,<br>
并依此实现系统初始化时服务的并行启动,<br>
同时达到降低 Shell 的系统开销的效果,<br>
最终代替现在常用的 System V 与 BSD 风格 init 程序.

Systemd 的功能包括:

0. 系统初始化: Systemd 负责系统的启动和关闭,包括加载内核模块、启动服务、以及初始化用户环境等.
0. 服务管理: Systemd 提供了统一的服务管理接口,可以用于启动、停止、重启、以及查看服务的状态等.
0. 设备管理: Systemd 提供了统一的设备管理接口,可以用于挂载文件系统、创建设备节点、以及管理设备的权限等.
0. 日志管理: Systemd 提供了统一的日志管理接口,可以用于收集和存储系统日志.
0. 时间管理: Systemd 提供了统一的时间管理接口,可以用于同步系统时钟、以及管理 NTP 服务等.

Systemd 已成为大多数 Linux 发行版的默认 init 程序,<br>
包括 Ubuntu、Fedora、Red Hat Enterprise Linux 等.

## 存放路径

0. /etc/systemd/system/ 目录下的服务是系统启动时自动加载的,不需要用户手动启用.
0. /usr/lib/systemd/system/ 目录下的服务是系统启动所必需的,不能被用户修改.
0. /usr/lib/systemd/user 目录下的服务仅在用户登录时启动,需要用户手动启用.

## 目标(*.target)

Systemd 目标是 Systemd 使用的抽象状态,用于描述系统的不同状态.<br>
目标可以表示系统的不同阶段,例如开机、关机、休眠或待机.<br>
目标还可以表示系统的不同功能,例如网络、文件系统或打印.

0. runlevel.target: 表示系统的不同运行级别.<br>
运行级别是系统的不同操作状态.<br>
默认运行级别是 2,表示多用户环境.
0. multi-user.target: 表示多用户环境.<br>
这是系统的默认运行级别.
0. graphical.target: 表示图形环境.
0. initramfs.target: 表示 initramfs 环境.<br>
initramfs 是一个预启动环境,用于初始化系统并加载内核模块.
0. rescue.target: 表示故障恢复环境.
0. poweroff.target: 表示关机.
0. reboot.target: 表示重启.
0. suspend.target: 表示休眠.
0. hibernate.target: 表示待机.

Systemd 还提供了许多其他目标,用于表示系统的不同状态或功能.<br>
可以使用 systemctl list-units 命令来查看系统中的所有目标.

## 查看服务日志

```sh
# 查看服务最后100行日志
journalctl -u 服务名 -n 100
```

# 关于Deepin桌面系统的系统初始化

> 通用/启动菜单/延时启动 -> 打开

只有当上面选项打开,自定义系统初始化配置才能成功运行.

# 语法

```conf
[Unit]
Description=test   		     # 简单描述服务
After=network.target       # 描述服务类别,表示本服务需要在network服务启动后在启动
Before=xxx.service         # 表示需要在某些服务启动之前启动,After和Before字段只涉及启动顺序,不涉及依赖关系.

[Service] 
Type=forking     		       # 设置服务的启动方式
User=USER 		             # 设置服务运行的用户
Group=USER		             # 设置服务运行的用户组
WorkingDirectory=/PATH	   # 设置服务运行的路径(cwd)
KillMode=control-group     # 定义systemd如何停止服务
Restart=no 		             # 定义服务进程退出后,systemd的重启方式,默认是不重启
ExecStart=/start.sh    	   # 服务启动命令,命令需要绝对路径(采用sh脚本启动其他进程时Type须为forking)
   
[Install]   
WantedBy=multi-user.target # 多用户
```

## 启动顺序与依赖关系定义(Unit)

### 服务描述

0. Description: 给出当前服务的简单描述
0. Documentation: 给出文档位置

### 启动顺序

0. After: 定义xxx.service应该在哪些target或service服务之后启动
0. Before: 定义xxx.service应该在哪些target或service服务之前启动

#### 目标状态

```sh
# 查看所有可用状态
systemctl --type=target --all
```

部分状态:

状态名|说明
-|-
network-pre.target|表示网络接口已启用,但尚未连接
network.target|表示网络已启用,但尚未连接,或者已连接但未进行身份验证
network-online.target|网络已连接
bluetooth.target|
cryptsetup.target|
emergency.target|
first-boot-complete.target|
getty-pre.target|
getty.target|
graphical.target|
initrd-root-fs.target|
initrd-usr-fs.target|
integritysetup.target|
local-fs-pre.target|
local-fs.target|
multi-user.target|
nfs-client.target|
nss-lookup.target|
nss-user-lookup.target|
paths.target|
remote-fs-pre.target|
remote-fs.target|
rescue.target|
rpcbind.target|
shutdown.target|
slices.target|
sockets.target|
sound.target|
swap.target|
sysinit.target|
time-set.target|
time-sync.target|
timers.target|
umount.target|
veritysetup.target|

### 依赖关系

0. Wants: 表示xxx.service与定义的服务存在“弱依赖”关系,即指定的服务启动失败或停止运行不影响xxx的允行
0. Requires: 则表示”强依赖”关系,即指定服务启动失败或异常退出,那么xxx也必须退出；反之xxx启动则指定服务也会启动

## 启动行为定义(Service)

### 启动命令

0. EnvironmentFile: 指定当前服务的环境参数文件(路径),如 EnvironmentFile=-/etc/sysconfig/xxx,连词号表示抑制错误,即发生错误时,不影响其他命令的执行
0. Environment: 后面接多个不同的shell变量,如Environment=DATA_DIR=/dir/data
0. User: 设置服务运行的用户
0. Group: 设置服务运行的用户组
0. WorkingDirectory: 设置服务运行的路径

Exec*: 各种与执行相关的命令

0. ExecStart: 定义启动服务时执行的命令
0. ExecStop: 定义停止服务时执行的命令
0. ExecStartPre: 定义启动服务前执行的命令
0. ExecStartPost: 定义启动服务后执行的命令
0. ExecStopPost: 定义停止服务后执行的命令
0. ExecReload: 定义重启服务时执行的命令

### 启动类型

Type: 字段定义启动类型,可以设置的值如下

0. simple(默认值): ExecStart字段启动的进程为主进程,即直接启动服务进程
0. forking: ExecStart字段将以fork()方式启动,此时父进程将会退出,子进程将成为主进程(例如用shell脚本启动服务进程)
0. oneshot: 类似于simple,但只执行一次,Systemd 会等它执行完,才启动其他服务
0. dbus: 类似于simple,但会等待 D-Bus 信号后启动
0. notify: 类似于simple,启动结束后会发出通知信号,然后 Systemd 再启动其他服务
0. idle: 类似于simple,但是要等到其他任务都执行完,才会启动该服务.<br>
一种使用场合是为让该服务的输出,不与其他服务的输出相混合

RemainAfterExit: 设为yes,表示进程退出以后,服务仍然保持执行

### 重启行为

KillMode: 定义 Systemd 如何停止服务,可以设置的值如下

0. control-group(default): 当前控制组里面的所有子进程,都会被杀掉
0. process: 只杀主进程
0. mixed: 主进程将收到 SIGTERM 信号,子进程收到 SIGKILL 信号
0. none: 没有进程会被杀掉,只是执行服务的 stop 命令

Restart: 定义了服务退出后,Systemd 的重启方式,可以设置的值如下(对于守护进程,推荐设为on-failure.对于那些允许发生错误退出的服务,可以设为on-abnormal)

0. no(default): 退出后不会重启
0. on-success: 只有正常退出时(退出状态码为0),才会重启
0. on-failure: 非正常退出时(退出状态码非0),包括被信号终止和超时,才会重启
0. on-abnormal: 只有被信号终止和超时,才会重启
0. on-abort: 只有在收到没有捕捉到的信号终止时,才会重启
0. on-watchdog: 超时退出,才会重启
0. always: 不管是什么退出原因,总是重启

RestartSec: 表示 Systemd 重启服务之前,需要等待的秒数

## 服务安装定义(Install)

WantedBy:表示该服务所在的 Target

Target的含义是服务组,如WantedBy=multi-user.target指的是该服务所属于multi-user.target.<br>
当执行systemctl enable xxx.service命令时,xxx.service的符号链接就会被创建在/etc/systemd/system/multi-user.target目录下.

可以通过systemctl get-default命令查看系统默认启动的target,一般为multi-user或者是graphical.<br>
因此配置好相应的WantedBy字段,可以实现服务的开机启动.

### multi-user.target和graphical.target

在 systemd 中,multi-user.target 和 graphical.target 是两个重要的 target.<br>
target 是 systemd 用来定义系统状态的概念.

multi-user.target 代表系统处于多用户模式.<br>
在多用户模式下,系统可以接受多个用户的登录.<br>
该模式通常用于服务器环境.

graphical.target 代表系统处于图形模式.<br>
在图形模式下,系统可以显示图形用户界面 (GUI).<br>
该模式通常用于桌面环境.

当系统启动时,它会首先启动 multi-user.target.<br>
当 multi-user.target 启动完成后,系统会根据 graphical.target 是否被启用来决定是否启动图形模式.

如果 graphical.target 被启用,则系统会启动图形服务,例如 X 服务器.<br>
如果 graphical.target 未被启用,则系统将保持在多用户模式下.

可以使用 systemctl isolate 命令来切换系统的 target.<br>
例如,要将系统切换到图形模式,可以使用以下命令:

```sh
sudo systemctl isolate graphical.target
```

要将系统切换到多用户模式,可以使用以下命令:

```sh
sudo systemctl isolate multi-user.target
```

# 参考资料

> [Zhiyu's Blog | 编写systemd服务脚本](https://qgrain.github.io/2020/05/12/%E7%BC%96%E5%86%99systemd%E6%9C%8D%E5%8A%A1%E8%84%9A%E6%9C%AC/)