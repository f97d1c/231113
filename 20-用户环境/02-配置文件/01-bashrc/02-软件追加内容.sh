#!/bin/bash --login
target_path="/home/$USER/.bashrc"

find -name 'bashrc追加内容' -type f -print0 | while IFS= read -r -d $'\0' file; do 
  dir_name=${file%/*}
  dir_1_name=${dir_name%/*}
  dir_2_name=${dir_name##*/}

  mark="$dir_2_name相关内容"
  [[ "$(cat $target_path)" =~ "$mark" ]] && continue;

  output $mark
  echo -e "\n\n# === $mark开始 ===" >> $target_path
  echo -e "\n$(cat $file)" >> $target_path
  echo -e "\n# === $mark结束 ===" >> $target_path
done