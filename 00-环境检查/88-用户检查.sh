# 目前只有容器执行该步骤
[[ "$(ps -p 1 -o comm=)" == 'systemd' ]] && return 8;

export USER='ubuntu'
# Docker中挂载目录时, 文件默认权限为
user_id=1000 # 用户ID是1000的用户所有
group_id=1000 # 用户组ID是1000的用户所有
user_name='ubuntu'
group_name='ubuntu'

error=''
if [ "$(grep $user_name: /etc/passwd)" ]; then
  return 8
elif [ "$(grep :$user_id: /etc/passwd)" ]; then
  error='用户ID已存在'
elif [ "$(grep $group_name: /etc/group)" ]; then
  error='用户组名已存在'
elif [ "$(grep :$group_id: /etc/group)" ]; then
  error='用户组ID已存在'
fi

[ "$error" ] && echo $error && return 255

echo '创建用户组'
sudo groupadd -g $group_id $group_name
[[ ! $? -eq 0 ]] && return 255

echo '创建用户'
sudo useradd --create-home -s /bin/bash -u $user_id -g $group_id $user_name

echo '设置用户密码'
echo "$user_name:ubuntu" | sudo chpasswd

echo 'soduers文件添加写权限'
sudo chmod u+w /etc/sudoers
sudo tee -a /etc/sudoers <<-EOF
%$user_name   ALL=(ALL)   NOPASSWD: ALL
EOF

# current_path=$PWD
# sudo su $user_name
# cd $current_path
