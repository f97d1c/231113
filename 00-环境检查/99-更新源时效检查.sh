# 容器环境跳过该步骤
[[ "$(ps -p 1 -o comm=)" != 'systemd' ]] && return 8;

# 获取文件的最后更新时间
last_update_time=$(stat -c %Y "/var/cache/apt/pkgcache.bin")
# 距离上次更新超过12小时则更新
[ $(($(($(date +%s) - last_update_time)) / (60*60*12))) -lt 1 ] && return 8

sudo apt update