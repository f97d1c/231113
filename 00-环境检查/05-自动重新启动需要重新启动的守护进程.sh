[[ ! "$(ps -p 1 -o comm=)" == 'systemd' ]] && return 8;

# 这个文件最开始没有 是后生成的
cnf_path='/etc/needrestart/needrestart.conf'
[ ! -f $cnf_path ] && return 8;

[[ ! "$(cat $cnf_path)" =~ '#$nrconf{restart}' ]] && return 8;

# i(交互式)、l(列出)或 a(自动)
sudo sed -i "s/#\$nrconf{restart} = 'i';/\$nrconf{restart} = 'a';/g" $cnf_path