printf '' > .app_export.sh

find . -type f -name '*.export' | sort -n | tr '\n' '\0' | while IFS= read -r -d $'\0' file; do 
  # output "加载变量: $file"
  file_name=${file##*/}
  var_name=${file_name%%.*}
  echo "export $var_name=$(cat $file | head -n 1)" >> .app_export.sh
done

cat .app_export.sh
source .app_export.sh