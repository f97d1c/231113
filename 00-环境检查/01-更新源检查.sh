# 目前只有容器需要做该项检查
[[ "$(ps -p 1 -o comm=)" == 'systemd' ]] && return 8;
[ "$(whoami)" != 'root' ] && sudo apt update && return 8;

sed -i 's/http:\/\/archive.ubuntu.com/https:\/\/mirrors.tuna.tsinghua.edu.cn/g' /etc/apt/sources.list
sed -i 's/http:\/\/security.ubuntu.com/https:\/\/mirrors.tuna.tsinghua.edu.cn/g' /etc/apt/sources.list

apt update
if [[ $(dpkg -l | grep ca-certificates 1>/dev/null 2>/dev/null; echo $?) -ne 0 ]];then
  apt install -y ca-certificates
  apt update
fi