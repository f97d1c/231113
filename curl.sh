#!/bin/bash --login
# curl -sL https://gitlab.com/f97d1c/231113/-/raw/master/curl.sh | bash

if [[ "$(whoami)" == 'root' ]];then
  install_path="/tmp/init"
else
  install_path="/home/${USER:-$(whoami)}/init"
fi

if [[ ! $(which 'git' 1>/dev/null 2>/dev/null; echo $?) -eq 0 ]];then
  sudo apt update
  sudo apt install -y git
fi

export GIT_MERGE_AUTOEDIT=no

if [ ! -e "$install_path" ];then
  git clone https://gitlab.com/f97d1c/231113.git "$install_path"
  cd "$install_path"
else
  cd "$install_path"
  git pull --quiet origin master
fi

bash bash.sh