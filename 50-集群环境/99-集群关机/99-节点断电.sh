[[ ! "$(hostname)" =~ "control.node" ]] && return 8;
[[ ! "$@" =~ '集群关机' ]] && return 8;

nodes=$(jq '.["节点"]' $(get_path_by_name '.机器设备.json'))
keys=($(jq -c -r '.| keys_unsorted[]' <<<$nodes))
for key in "${keys[@]}";do 
  [[ $(ip addr | grep $key >/dev/null; echo $?) -eq 0 ]] && continue;
  detail=$(jq -r --arg key $key '.["\($key)"]' <<<$nodes)
  ipv4_addr=$(jq -r '.["IPV4地址"]' <<<$detail)
  user_name=$(jq -r '.["系统用户名"]//"ubuntu"' <<<$detail)
  
  output "节点断电: $ipv4_addr"
  cluster_execute_cmd $user_name $ipv4_addr "sudo shutdown now" 1>/dev/null 2>/dev/null;
  code=$?; [[ $code -ne 255 ]] && output "[$code]关机命令执行失败." && return 233
  output "节点关机."
done