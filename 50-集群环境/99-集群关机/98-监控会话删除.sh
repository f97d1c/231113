[[ ! "$(hostname)" =~ "control.node" ]] && return 8;
[[ $(tmux ls 2>/dev/null | grep 'cluster' 1>/dev/null 2>/dev/null; echo $?) -ne 0 ]] && return 8;
[[ ! "$@" =~ '集群关机' ]] && return 8;

tmux kill-session -t cluster