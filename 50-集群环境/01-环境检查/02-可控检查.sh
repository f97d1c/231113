[[ ! "$(hostname)" =~ "control.node" ]] && return 8;
[[ ! "$@" =~ '集群初始化' ]] && return 8;

nodes=$(jq '.["节点"]' $(get_path_by_name '.机器设备.json'))
keys=($(jq -c -r '.| keys_unsorted[]' <<<$nodes))
for key in "${keys[@]}";do
  detail=$(jq -r --arg key $key '.["\($key)"]' <<<$nodes)
  ipv4_addr=$(jq -r '.["IPV4地址"]' <<<$detail)
  user_name=$(jq -r '.["系统用户名"]//"ubuntu"' <<<$detail)
  
  output "可控检查: $ipv4_addr"
  cluster_execute_cmd $user_name $ipv4_addr "echo 'Hello Word.'"  1>/dev/null 2>/dev/null;
  [[ $? -ne 0 ]] && output "控制命令执行失败." && return 233
  output "节点可控."
done

output "可控检查: 完成."