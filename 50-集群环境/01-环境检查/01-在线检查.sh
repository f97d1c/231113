[[ ! "$(hostname)" =~ "control.node" ]] && return 8;
[[ ! "$@" =~ '集群初始化' ]] && return 8;

nodes=$(jq '.["节点"]' $(get_path_by_name '.机器设备.json'))
keys=($(jq -c -r '.| keys_unsorted[]' <<<$nodes))
for key in "${keys[@]}";do
  detail=$(jq -r --arg key $key '.["\($key)"]' <<<$nodes)
  ipv4_addr=$(jq -r '.["IPV4地址"]//"false"' <<<$detail)
  output "在线检查: $ipv4_addr"
  [[ $ipv4_addr == 'false' ]] && output "IPV4地址为空." && return 233

  nmap -p 22 $ipv4_addr 1>/dev/null 2>/dev/null
  [[ $? -ne 0 ]] && output "节点联系失败." && return 233

  output "节点在线."
done

output "在线检查: 完成."

# source 50-集群环境/01-环境检查/02-可控检查.sh