# [ ${LANG:-''} == 'zh_CN.UTF-8' ] && return 8;

# sudo apt install -fy locales
# sudo locale-gen zh_CN.UTF-8
# sudo apt install -y language-pack-zh-han*

# [[ $(physical_host?) -eq 0 ]] && sudo localectl set-locale LANG=zh_CN.UTF-8

# return 0

# [[ $(physical_host?) -ne 0 ]] && [[ ${LANG:-''} == 'zh_CN.UTF-8' ]] && return 8;

return 8 # 目前安装后$LANGUAGE还是为空 远程中文没问题 本地登录却是乱码问题尚未解决
# 尝试当宿主机中文显示正常 容器内无配置情况下会怎样
[[ $(physical_host?) -ne 0 ]] && return 8;

[[ ${LANGUAGE:-''} =~ 'zh_CN' ]] && return 8;

sudo apt install -fy locales
sudo apt install -y language-pack-zh-han*

sudo tee -a /etc/environment <<-'EOF'
LANG="zh_CN.UTF-8"
LANGUAGE="zh_CN:zh:en_US:en"
EOF

sudo tee -a /var/lib/locales/supported.d/local <<-'EOF'
en_US.UTF-8 UTF-8
zh_CN.UTF-8 UTF-8
zh_CN.GBK GBK
zh_CN GB2312
EOF

sudo locale-gen