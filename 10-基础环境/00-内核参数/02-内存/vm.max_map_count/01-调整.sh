#!/bin/bash --login
max_map_count=262144

[ $(cat /proc/sys/vm/max_map_count) -eq $max_map_count ] && return 8;

sudo sysctl -w vm.max_map_count=$max_map_count