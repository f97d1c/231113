timezone='Asia/Shanghai'
[ $(cat /etc/timezone) == $timezone ] && return 8;

# 这种方法需要手动选择时区
# [[ ! $(installed? 'timedatectl') -eq 0 ]] && sudo apt install -y tzdata;
# sudo timedatectl set-timezone $timezone
# output $(date +%Y年%m月%d日%H时%M分%S秒)

localtime="/usr/share/zoneinfo/$timezone"
output "建立期望的时区链接($localtime)"
sudo ln -fs $localtime /etc/localtime
echo "Asia/Shanghai" | sudo tee /etc/timezone
[[ ! $(installed? 'timedatectl') -eq 0 ]] && sudo apt install -y tzdata;
output '时区设置完成'
output $(date +%Y年%m月%d日%H时%M分%S秒)